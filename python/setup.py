#!/usr/bin/env python

from setuptools import setup, find_packages

f = open("../README.md", "r")
long_description = f.read()
f.close()

setup(
	name='pygar',
	version='1.0.5',
	description='Searches the SEC\'s Edgar database using a variety of methods',
	url='https://bcable.net/x/pygaR',
	author='Brad Cable',
	author_email='brad@bcable.net',
	license='MIT',
	long_description=long_description,
	long_description_content_type="text/markdown",
	classifiers=[
		'Development Status :: 5 - Production/Stable',
		'Intended Audience :: Science/Research',
		'Topic :: Office/Business :: Financial :: Investment',
		'Topic :: Scientific/Engineering :: Information Analysis',
		'License :: OSI Approved :: MIT License',
		'Programming Language :: Python :: 2',
		'Programming Language :: Python :: 2.7',
		'Programming Language :: Python :: 3',
		'Programming Language :: Python :: 3.3',
		'Programming Language :: Python :: 3.4',
		'Programming Language :: Python :: 3.5',
	],
	keywords='pygar edgar sec stock filing',
	packages=find_packages(),
	install_requires=['certifi', 'pycurl']
)
