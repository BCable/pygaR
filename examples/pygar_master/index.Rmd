pygaR: Example for pygar\_master() Function
===========================================

## Load Libraries

```{r libraries}
library(pygaR)
library(ggplot2)
```

## Setup Default ggplot2 Theme

```{r default_theme}
d <- theme_bw()
d <- d + theme(
	axis.text.x = element_text(angle=90, size=15),
	axis.title = element_text(size=20),
	plot.title = element_text(size=30)
)
def_theme <- d
```

## Data Analysis

### Gather All Data

```{r master_data,cache=TRUE}
master_data <- pygar_master(startqtr=199301, endqtr=201701)
simpler_master_data <- data.frame(
	Year.Quarter=factor(master_data$Quarter),
	CIK=master_data$CIK, Form.Type=factor(master_data$Form.Type),
	Date.Filed=master_data$Date.Filed
)
master_data <- simpler_master_data
master_data$Day <- substr(master_data$Date.Filed, 6, 10)
```

### Aggregate Filings by All Quarters

```{r master_agg_length}
master_agg_length <- aggregate(CIK ~ Year.Quarter, data=master_data, FUN=length)

names(master_agg_length) <- c("Year.Quarter", "Filings")
master_agg_length$Year <- factor(substr(master_agg_length$Year.Quarter, 0, 4))
master_agg_length$Quarter <- factor(
	substr(master_agg_length$Year.Quarter, 6, 7)
)
```

### Filings Bar Chart by All Quarters

```{r master_agg_length_bar,fig.height=10,fig.width=15}
g <- ggplot(master_agg_length, aes(x=Year.Quarter, y=Filings/1000, group=1))
g <- g + geom_bar(stat="identity")
g <- g + def_theme
g <- g + ylab("Filings (Thousands)")
g <- g + ggtitle("Filings by All Quarters")
g
```

### Filings Line Graph by All Quarters

```{r master_agg_length_line,fig.height=10,fig.width=15}
g <- ggplot(master_agg_length, aes(x=Year.Quarter, y=Filings/1000, group=1))
g <- g + geom_line() + geom_point()
g <- g + def_theme
g <- g + theme(axis.text.x = element_text(angle=90, size=12))
g <- g + ylab("Filings (Thousands)")
g <- g + ggtitle("Filings by All Quarters")
g
```

### Filings Line Graph by Year Grouped by Quarter

```{r master_agg_length_qtr_line,fig.height=10,fig.width=15}
g <- ggplot(master_agg_length,
	aes(x=Year, y=Filings/1000, group=Quarter, colour=Quarter)
)
g <- g + geom_line() + geom_point()
g <- g + def_theme
g <- g + ylab("Filings (Thousands)")
g <- g + ggtitle("Filings by Year Grouped by Quarter")
g
```

### Aggregate Unique CIK by All Quarters
```{r master_agg_unique}
master_agg_unique <- aggregate(CIK ~ Year.Quarter, data=master_data, FUN=unique)
master_agg_unique$CIK <- as.integer(as.vector(unlist(
	lapply(master_agg_unique$CIK, FUN=length)
)))

names(master_agg_unique) <- c("Year.Quarter", "Unique.CIK")
master_agg_unique$Year <- factor(substr(master_agg_unique$Year.Quarter, 0, 4))
master_agg_unique$Quarter <- factor(
	substr(master_agg_unique$Year.Quarter, 6, 7)
)
```

### Unique CIK Line Graph Grouped by Quarter

```{r master_agg_unique_qtr_line,fig.height=10,fig.width=15}
g <- ggplot(master_agg_unique,
	aes(x=Year, y=Unique.CIK, group=Quarter, colour=Quarter)
)
g <- g + geom_line() + geom_point()
g <- g + def_theme
g <- g + ggtitle("Unique CIK Grouped by Quarter")
g
```

### Aggregate Filings by Day of Year

```{r master_aggday_length}
master_aggday_length <- aggregate(CIK ~ Day, data=master_data, FUN=length)
names(master_aggday_length) <- c("Day", "Filings")
```

### Filings Line Graph by Day of Year

```{r master_aggday_length_line,fig.height=10,fig.width=15}
g <- ggplot(master_aggday_length, aes(x=Day, y=Filings, group=1))
g <- g + geom_line() + geom_point()
g <- g + def_theme + theme(axis.text.x = element_blank())
g <- g + ggtitle("Filings by Day of Year")
g
```

### Aggregate Form Type by All Quarters

```{r master_formagg_unique}
master_formagg_length <- aggregate(
	CIK ~ Form.Type, data=master_data, FUN=length
)

names(master_formagg_length) <- c("Form.Type", "Filings")
```

### Form Type Frequencies

```{r master_formagg_unique_bar,fig.height=10,fig.width=15}
g <- ggplot(master_formagg_length, aes(x=Form.Type, y=Filings/1000), FUN=length)
g <- g + geom_bar(stat="identity")
g <- g + def_theme
g <- g + ylab("Filings (Thousands)")
g <- g + ggtitle("Form Type Frequencies")
g
```

### Total Form Types

```{r total_formtype}
length(master_formagg_length[,1])
```

&lt;insert commentary about wall street here&gt;

### All Forms
```{r formtypes}
master_formagg_length$Form.Type
```

### Look at Top 5% of Form Types

```{r simpler_formtype}
master_formagg_length <- master_formagg_length[
	order(-master_formagg_length$Filings),
]
bottom_value <- master_formagg_length$Filings[
	length(master_formagg_length$Filings) * 0.05
]
master_formagg_length <- master_formagg_length[
	master_formagg_length$Filings > bottom_value,
]
```

### Top 5% Form Type Bar Chart

```{r master_formagg_top5perc_bar,fig.height=10,fig.width=15}
g <- ggplot(master_formagg_length, aes(x=Form.Type, y=Filings/1000), FUN=length)
g <- g + geom_bar(stat="identity")
g <- g + def_theme
g <- g + theme(axis.text.x = element_text(angle=90, size=12))
g <- g + ylab("Filings (Thousands)")
g <- g + ggtitle("Top 5% of Form Type Frequencies")
g
```
