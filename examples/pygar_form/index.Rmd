pygaR: Example for pygar\_form() Function
=========================================

## Load Libraries

```{r libraries}
library(pygaR)
library(ggplot2)
library(stringr)
library(XML)
```

## Setup Default ggplot2 Theme

```{r default_theme}
d <- theme_bw()
d <- d + theme(
	axis.text.x = element_text(angle=90, size=15),
	axis.title = element_text(size=20),
	plot.title = element_text(size=30)
)
def_theme <- d
```

https://www.bloomberg.com/news/articles/2017-04-28/alphabet-loves-google-ceo-so-much-he-gets-hundreds-of-millions

https://www.wsj.com/articles/google-ceo-tops-other-alphabet-execs-with-200-million-pay-1493424255

Mentions the day "Friday", translating to 2017-04-28

## Data Analysis

### Search for Filings That Day

```{r find_filing,cache=TRUE}
filings <- pygar_master(date=20170428, company='/alphabet inc/i')
filings
```

### Grab Form Information

```{r get_form,cache=TRUE}
form <- pygar_form(filings$File.Name[1])
```

### Show Some Basic Form Information

```{r form_names}
names(form)
```

```{r form_names_header}
names(form$Header)
```

```{r form_names_body_doc1}
names(form$Body[[1]])
```

### Find a Relevant Document

```{r document_find}
grepl('pichai', form$Body, ignore.case=TRUE) &
	grepl('schmidt', form$Body, ignore.case=TRUE)
```

### Grab Relevant Document

```{r document_grab}
doc <- form$Body[[1]]
```

### Write To File

```{r document_writefile}
writeLines(doc$Text, file("alphabet.html"))
```

[alphabet.html](alphabet.html)

### Display Summary Compensation Table

```{r sct_xmlparse}
sct_xmlparse <- function(html, xpath){
	html_obj <- htmlParse(html)
	xpathApply(html_obj, xpath, xmlValue)
}
```

```{r sct_grab}
sct_grab <- function(data){
	#data <- gsub("\n", "", data, fixed=TRUE)
	data <- str_trim(data)
	data <- strsplit(data, "<TABLE")[[1]]
	for(table in data){
		table <- paste0(
			"<TABLE", strsplit(table, "</TABLE>")[[1]][1], "</TABLE>",
			collapse=""
		)

		if(!is.na(
			grep("Salary", table) &&
			grep("Bonus", table) &&
			grep("Principal", table)
		)){
			data <- table
			break
		}
	}

	data <- gsub("(<[A-Z]+)[ \n][^>]+\">", "\\1>", data, ignore.case=TRUE)
	data <- gsub(">[\n ]+<", "><", data)
	data <- gsub(
		"<[\\/]?(?:(?!(?:TABLE|TR|TH|TD|P|BR))[^>])+>", "", data,
		perl=TRUE, ignore.case=TRUE
	)

	data
}
```

```{r sct_display,results="asis"}
doc_table <- sct_grab(doc$Text)
doc_table
```

### Single Parse Out

```{r sct_table_df}
sct_table_df <- function(doc_table){
	
}
```

```{r sct_parse}
sct_parse <- function(doc_text){
	doc_table <- sct_grab(doc_text)
	doc_df <- sct_table_df(doc_table)
}
```

```{r sct_parse_display}
sct_parse(doc$Text)
```

```{r grab_alphabet,cache=TRUE}
alphabet_data <- pygar_master(
	startqtr=201001, endqtr=201604,
	cik=1652044, form="DEF 14A"
)
```

```{r display_alphabet}
alphabet_data
```

### Get Google CIK

```{r grab_google_cik,cache=TRUE}
google_one <- pygar_master(qtr=201001, company='/google inc/i', form="DEF 14A")
google_cik <- google_one$CIK[1]
google_cik
```

### Grab More Google Data

```{r grab_google_data,cache=TRUE}
google_data <- pygar_master(
	startqtr=200201, endqtr=201504,
	cik=google_cik, form="DEF 14A"
)
```

```{r display_google_data}
google_data
```

### Grab Google Forms

```{r grab_google_docs,cache=TRUE}
google_docs <- NULL
for(file in google_data$Filename){
	google_docs <- c(google_docs,
		pygar_form(file)$Body[[1]]$Text
	)
}
```

```{r display_google_docs,results="asis"}
sct_grab(google_docs[10])
```

Still figuring out a good way to gather this info for a graph...
