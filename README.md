pygaR 1.0.5
===========

pygaR is a Python module (with R package wrapper) that searches the SEC EDGAR
system in a variety of methods.  It is cross platform (Linux/UNIX/Windows), and
works on either the Python 2 or 3 branches.

More information on the SEC EDGAR system can be found here:

https://www.sec.gov/edgar.shtml

This project is a module on PyPI:

https://bcable.net/x/pygaR/pypi

This project's source code can be found here:

https://bcable.net/x/pygaR

This project's Sphinx source code can be found here:

https://bcable.net/x/pygaR/sphinx

## Examples

[pygar\_form(...) Example](https://bcable.net/analysis-pygar_form.html)

[pygar\_master(...) Example](https://bcable.net/analysis-pygar_master.html)

[pygar\_search(...) Example](https://bcable.net/analysis-pygar_search.html)

## System Requirements

Python 2.7+, 3.3+

R (not required): 3+

## Installation

### Python Module

#### Linux/UNIX

    pip install pygar

#### Windows

    path\to\python.exe -m pip install pygar

### R Package

Requires devtools package to install, and the pygar Python module in order to function.

If you don't have the "devtools" package installed in R, you will need to
install that first:

    install.packages("devtools")

To install the R package:

    library(devtools)
    install_git("https://gitlab.com/BCable/pygaR.git", subdir="Rpkg")

### Backup Mirror Links

https://gitlab.com/BCable/pygaR

https://bcable.gitlab.io/pygaR/sphinx

https://bcable.gitlab.io/pygaR/pygar_form/

https://bcable.gitlab.io/pygaR/pygar_master/

https://bcable.gitlab.io/pygaR/pygar_search/

https://pypi.python.org/pypi/pygar
