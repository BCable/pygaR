.. pygaR documentation master file, created by
   sphinx-quickstart on Wed May  3 19:39:29 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for pygaR
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

pygaR is a Python module (with R package wrapper) that searches the SEC EDGAR
system in a variety of methods.  It is cross platform (Linux/UNIX/Windows), and
works on either the Python 2 or 3 branches.

More information on the SEC EDGAR system can be found here:

https://www.sec.gov/edgar.shtml

This project is a module on PyPI:

https://bcable.net/x/pygaR/pypi

This project's source code can be found here:

https://bcable.net/x/pygaR

This project's Sphinx source code can be found here:

https://bcable.net/x/pygaR/sphinx


Requirements
============

Python 2.7+, 3.3+

PyPI Modules: certifi, pycurl


Installation
============

Easy installation can be done via the 'pip' command:

   pip install pygar

Or on Windows:

   \\path\\to\\python.exe install pygar

After installation a simple 'import pygar' command will get you started.


Usage
=====

.. autofunction:: pygar.form

.. autofunction:: pygar.master

.. autofunction:: pygar.search


Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`


Backup Mirror Links
===================

https://gitlab.com/BCable/pygaR

https://bcable.gitlab.io/pygaR/sphinx

https://bcable.gitlab.io/pygaR/pygar_form/

https://bcable.gitlab.io/pygaR/pygar_master/

https://bcable.gitlab.io/pygaR/pygar_search/

https://pypi.python.org/pypi/pygar
