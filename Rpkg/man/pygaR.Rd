\name{pygaR}
\alias{pygaR}
\alias{pygaR::pygar_form}
\alias{pygaR::pygar_master}
\alias{pygaR::pygar_search}
\alias{pygar_form}
\alias{pygar_master}
\alias{pygar_search}
\title{SEC EDGAR database retrieval and search}
\description{Searches the SEC's Edgar database using a variety of methods}
\usage{
pygar_form(path)
pygar_master(qtr=NULL, startqtr=NULL, endqtr=NULL, date=NULL, cik=NULL, form=NULL, company=NULL, filename=NULL)
pygar_search(query=NULL, qtr=NULL, startqtr=NULL, endqtr=NULL, date=NULL, cik=NULL, form=NULL, company=NULL, filename=NULL)
}
\arguments{
\item{path}{The path of the EDGAR form starting with "edgar/"}

\item{date}{Specific date to query in format 20170504}

\item{qtr}{Specific quarter to query in format 201702}

\item{startqtr}{Beginning of a quarter range in format 201701}

\item{endqtr}{Beginning of a quarter range in format 201702}

\item{cik}{A CIK is an SEC specific unique company identifier}

\item{company}{This searches for a company name}

\item{form}{Form filing type such as "4", "10-K", "10-Q", "DEF 14A", etc}

\item{date_filed}{Date the form was filed}

\item{filename}{Filename in the EDGAR system}
}
\value{
pygar_form() returns structured form data from a single form

pygar_master() returns data frame of data returned based on specifiers

pygar_search() returns data frame of master data plus queried data tacked on
}
\author{Brad Cable}
\examples{
filings_in_2016 <- pygar_master(startqtr=201601, endqtr=201604)

search_for_alphabet_filing <- filings <- pygar_master(date=20170428, company='/alphabet inc/i')

get_alphabet_form <- pygar_form('edgar/data/1652044/0001308179-17-000170.txt')

# warning: large amounts of data pulled
query_10k_filings <- pygar_search(
    startqtr=201601, endqtr=201604, form="10-K", header=TRUE,
    query=list(
        Filer=list(
            Company.Data=list(
                State.Of.Incorporation='State.Of.Incorporation',
                Standard.Industrial.Classification=
                    'Standard.Industrial.Classification'
            ),
            Business.Address=list(
                State='State'
            )
        )
    )
)

}
\keyword{pygar}
\keyword{python-edgar}
\keyword{edgar}
