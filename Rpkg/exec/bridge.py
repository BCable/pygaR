#!/usr/bin/env python3

#
# pygaR
#
# Author: Brad Cable
# Email: brad@bcable.net
# License: MIT
#


from copy import deepcopy
import sys

try:
	import pygar
except ImportError:
	print('error,pygaR Python module not found.')
	sys.exit(1)


if __name__ == '__main__':
	# get arguments from cmdline
	args = deepcopy(sys.argv)
	args.pop(0)

	# get command to execute
	command = args.pop(0)
	cmd_obj = pygar.__getattribute__(command)

	# start gathering keyword arguments
	kwargs = {}
	try:
		while True:
			name = args.pop(0)
			val = args.pop(0)
			kwargs[name] = val
	except IndexError:
		pass

	# execute
	try:
		print(cmd_obj(**kwargs))
	except pygar.PygarException as e:
		print('error,Python Module Exception: {}'.format(e))
		sys.exit(1)
	except Exception as e:
		print('error,Unknown Python Exception:{}'.format(e))
		sys.exit(1)
